+++
title = "Bienvenue sur Jabber"
+++

Bienvenue dans la communauté Jabber. Sur cette page, nous t'aiderons à trouver un serveur et un client pour rejoindre le réseau et chatter avec tes amiEs, sans fuiter toutes tes données. Jabber est décentralisé, comme l'email. Cela veut dire que tu dois trouver un serveur, et tu pourras ensuite parler avec tout le monde.

Cela peut être un peu compliqué au départ, mais les addresses Jabber (JID) ressemblent aux adresses email. Par exemple, si tu as le compte `louise.michel` sur le serveur jabber.fr, ton adresse sera `louise.michel@jabber.fr`. Les salons de discussions (aussi appelés MUCs) ont des adresses similaires, comme `chat@joinjabber.org`.

Allez, c'est parti !

# Étape 1 : Créer un compte sur un serveur

Selon tes besoins, nous pouvons recommander différents serveurs. Clique sur ton utilisation pour en savoir plus.

<div class="usecases" style="text-align: center">
<details>
<summary>
{{ usecase(usecase="personal") }}
</summary>
<p>Pour tes besoins personnels, tu peux chercher un hébergeur sans profits avec un modèle économique durable, qui a des chances d'être encore là dans 20 ans. Nous recommandons :</p>
{{ server(usecase="personal") }}
</details>
<details>
<summary>
{{ usecase(usecase="collective") }}
</summary>
<p>Pour ton collectif/organisation, tu peux <a href="https://homebrewserver.club/category/instant-messaging.html">mettre en place ton propre serveur</a>, par exemple avec <a href="https://yunohost.org/">Yunohost</a>. Si c'est hors de portée pour toi, nous recommandons les hébergeurs professionels suivants :</p>
{{ server(usecase="collective") }}
</details>
<details>
<summary>
{{ usecase(usecase="pseudo") }}
</summary>
<p>Pour tes activités politiques, il y a de nombreux serveurs fournissant des comptes Jabber pseudonymes. En voici quelques uns :</p>
{{ server(usecase="pseudo") }}
</details>
</div>

# Étape 2 : Choisis un client

Maintenant que tu as un compte sur un serveur, tu peux télécharger et configurer un client. Voici une liste de clients que nous recommandons pour leur qualité et leur dédication aux besoins de la communauté. Clique sur ton système d'exploitation pour être emmenéE vers une page de documentation dédiée.

<div class="platforms" style="text-align: center">
{{ platform(platform="android") }}
{{ platform(platform="ios") }}

{{ platform(platform="gnulinux") }}
{{ platform(platform="windows") }}
{{ platform(platform="macos") }}
</div>

# Étape 3 : Tu y es !

Félicitations ! Tu as maintenant un serveur est un client. Tu peux ouvrir ton client et lui donner ton adresse Jabber pour te connecter au serveur et commencer à chatter avec des amiEs. Si tu te sens un peu seulE, tu peux rejoindre le chat de notre communauté : [chat@joinjabber.org](xmpp:chat@joinjabber.org?join)
